<?php

namespace App\Controller;

use App\Entity\Article;
use App\Repository\ArticleRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class BlogController extends AbstractController
{
    #[Route('/blog', name: 'blog')]
    public function index(ArticleRepository $repo): Response
    {
        $article = $repo->findAll();
        return $this->render('blog/index.html.twig', [
            'controller_name' => 'BlogController',
            'articles' => $article
        ]);
    }

    #[Route('/', name: 'home')]
    public function home(){
        return $this->render('blog/home.html.twig');
    }


    #[Route('/blog/new', name: 'blog_create')]
    public function create()
    {
        $article = new Article();
         // Créer un formulaire en symfony lié à mon article
         $form = $this->createFormBuilder($article)
                      ->add('title')
                      ->add('content')
                      ->add('image')
                      ->getForm();
        return $this->render('blog/create.html.twig', [
            'formArticle' => $form->createView()
        ]);
    }

    #[Route('/blog/{id}', name: 'blog_show')]
    public function show(Article $article){
        return $this->render('blog/show.html.twig', [
            'article' => $article
        ]);
    }
    
}
